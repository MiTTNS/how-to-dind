To get a Docker in Docker (dind) runner working you’ll need to set the runner up with some specific options.  
Here’s a boiler plate version of the command:

```
sudo gitlab-runner register -n \
    --url <URL> \
    --registration-token <TOKEN> \
    --description "$(id -un)@$(hostname) \
    --tag-list docker,tls \
    --executor docker \
    --docker-image alpine:latest \
    --docker-privileged \
    --docker-volumes /cert/client
```

The <URL> and <TOKEN> tags will need to be replaced with your gitlab url and token.  These can be found in your 
projects settings, under the CI/CD heading.  Once under the CI/CD heading expand the runners tab and look for 
the url and token under Specific Runners.  

<img src="howToDinD7.png">

In the command above take note of the –tag-list option.  We’ve assigned this runner two tags, docker and tls, and 
we’ll need to include these in our CI jobs. 
We’ll also need a dind sidecar.  The sidecar is a second container that is connected to the main one we’re running.  
In our case, the sidecar will run the Docker daemon that we access from our container every time we use a Docker 
command.  This is done using the service heading in gitlab-ci.yml.  
Here’s what one of these jobs could look like:

```yaml
first:
    stage: first
    tags:
        - docker
        - tls
    services:
        - docker:19.03-dind
    image: docker:19.03
    script:
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - docker build --pull -t "$CI_REGISTRY_IMAGE" .
        - docker images
```

The main things to note here are the tags, services, and image.  Tags references the tags we gave the 
runner, it’s important that the job have the same tags as the runner.  Services is what creates the sidecar 
for the Docker daemon and will act like a background process.  Finally, we’re using the Docker image so 
we have access to the Docker commands we need to access our Docker sidecar.  

What you do in the script section is kind of up to you.  Here I’ve build a Docker image from a Dockerfile 
that is in the root directory of the project and called docker images to check that it’s there.  

PITFALLS/NOTES:
 - if the `docker login` step fails, try echoing `$CI_REGISTRY`
 - if you're still new to docker and gitlab CI/CD (like me) this is actually a really good example of services
   and the docker CI/CD executor, the idea is that each container runs one process but for docker to function 
   you need a daemon for it running in the background; to allow both of these conditions to be met we run the 
   daemon in its service container and reach out to it with docker commands in our container